## Nick Gray

Infinit scroll Wordpress theme with flickering splash page.

This repo contains the whole WP install. If you already have a local version of Wordpress running then simply copy the contents of the repo's themes directory into your local themes directory. You will then need run the steps below to compile the SASS.

### Gulp Stuff

You will need to install dependancies to run the Gulp tasks. Navigate to the root of the theme in your CLI and run the following.

```
npm install
```
Run Gulp to process your SASS and start the watch task.

```
gulp
```

### CSS Autoprefixer

PostCSS plugin to parse CSS and add vendor prefixes to CSS rules using values from Can I Use.

### Normalize CSS Reset

This template uses Normalize.css. This makes browsers render all elements more consistently and in line with modern standards. It precisely targets only the styles that need normalizing.
